package org.eugene.library.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eugene.library.entity.Quiz;
import org.eugene.library.entity.Result;
import org.eugene.library.entity.User;
import org.eugene.library.repositories.ResultRepository;
import org.eugene.library.repositories.UserDetailsRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ResultService {
    private final ResultRepository resultRepository;
    private final UserDetailsRepo userDetailsRepo;

    public Result save(Result result) {
        return resultRepository.save(result);
    }

    public void deleteAll() {
        resultRepository.deleteAll();
    }

    public List<Result> getUserResults(String userUid) {
        List<Result> results = new ArrayList<>();
        Optional<User> user = userDetailsRepo.findById(userUid);
        if (user.isPresent()) {
            results = resultRepository.getByUser(user.get());
        }
        return results;
    }

    @Transactional
    public void deletePreviousResult(User user, Quiz quiz) {
        resultRepository.deleteByUserAndQuiz(user, quiz);
    }
}
