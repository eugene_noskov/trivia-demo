package org.eugene.library.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eugene.library.entity.*;
import org.eugene.library.repositories.UserDetailsRepo;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class AnswerService {

    private final QuestionService questionService;
    private final QuizService quizService;
    private final UserDetailsRepo userDetailsRepo;
    private final ResultService resultService;

    public Result saveQuizAnswersResult(String userId, long quizId, List<Question> userAnswers) {
        Map<Long, Boolean> quizQuestions = questionService.getQuizAnswers(quizId);
        Quiz quiz = quizService.getQuizById(quizId);

        if (!validateAnswers(quiz, quizQuestions, userAnswers)) {
            log.warn("Validation is wrong for user {} quiz id {}", userId, quizId);
            return null;
        }

        Optional<User> user = userDetailsRepo.findById(userId);
        if (!user.isPresent()) {
            log.warn("User with id {} is not found", userId);
            return null;
        }

        Result result = createResult(user.get(), quiz, quizQuestions, userAnswers);
        if (result == null) {
            return null;
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        resultService.deletePreviousResult(user.get(), quiz);
        result = resultService.save(result);
        result.setUser(null);

        try {
            String someJsonString = mapper.writeValueAsString(result);
            result = mapper.readValue(someJsonString, Result.class);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    private Result createResult(User user, Quiz quizId, Map<Long, Boolean> quizQuestionAnswers, List<Question> userAnswers) {

        int correctAnswers = 0;
        for (Question userAnswer : userAnswers) {
            Set<Option> options = userAnswer.getOptions();
            boolean answerIsCorrect = true;
            for (Option option : options) {
                if (quizQuestionAnswers.get(option.getId()) != option.getIsCorrect()) {
                    answerIsCorrect = false;
                    break;
                }
            }
            if (answerIsCorrect) {
                correctAnswers++;
            }
        }

        Result result = new Result();
        result.setUser(user);
        result.setQuiz(quizId);
        result.setRightAnswers(correctAnswers);
        result.setWrongAnswers(userAnswers.size() - correctAnswers);

        return result;
    }

    private boolean validateAnswers(Quiz quizId, Map<Long, Boolean> quizAnswers, List<Question> userAnswers) {
        int numUserAmswers = 0;
        for (Question userAnswer : userAnswers) {
            numUserAmswers += userAnswer.getOptions().size();
        }

        if (quizId == null || numUserAmswers != quizAnswers.size()) {
            return false;
        }

        for (Question question : userAnswers) {
            for (Option option : question.getOptions()) {
                Boolean currentAnswer = quizAnswers.get(option.getId());
                if (currentAnswer == null) {
                    return false;
                }
            }
        }
        return true;
    }
}
