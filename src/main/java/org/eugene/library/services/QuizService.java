package org.eugene.library.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eugene.library.entity.Quiz;
import org.eugene.library.repositories.QuizRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuizService {
    private final QuizRepository quizRepository;

    public Quiz saveQuiz(Quiz quiz) {
        Quiz save = null;
        try {
            save = quizRepository.save(quiz);
        } catch (Exception e) {
            log.error("Can't save question {}", e.getMessage());
        }
        return save;
    }

    public List<Quiz> getQuizzes() {
        return quizRepository.findAll();
    }

    public void deleteAll() {
        quizRepository.deleteAll();
    }

    public Quiz getQuizById(long quizId) {
        Optional<Quiz> quiz = quizRepository.findById(quizId);
        if (quiz.isPresent()) {
            return quiz.get();
        }
        return null;
    }
}
