package org.eugene.library.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eugene.library.entity.Option;
import org.eugene.library.entity.Question;
import org.eugene.library.entity.Quiz;
import org.eugene.library.repositories.QuestionRepository;
import org.eugene.library.repositories.QuizRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuestionService {
    private final QuizRepository quizRepository;
    private final QuestionRepository questionRepository;

    @Transactional
    public Question saveQuestion(long quizId, @Valid Question question) {
        Question save = null;

        Optional<Quiz> quizOpt = quizRepository.findById(quizId);
        if (quizOpt.isPresent()) {

            question.setQuiz(quizOpt.get());

            if (question.getOptions() != null) {
                for (Option option : question.getOptions()) {
                    option.setQuestion(question);
                }
            }
            try {
                save = questionRepository.save(question);
            } catch (Exception e) {
                log.error("Can't save question {}", e.getMessage());
            }
        }
        return save;
    }

    public List<Question> getQuizQuestions(long quizId) {
        List<Question> allByQuizId = questionRepository.findAllByQuizId(quizId);
        allByQuizId.forEach(q -> q.getOptions().forEach(o -> o.setIsCorrect(false)));
        return allByQuizId;
    }

    public void deleteAll() {
        questionRepository.deleteAll();
    }

    public void saveQuestion(Question question) {
        saveQuestion(question.getQuiz().getId(), question);
    }

    public void saveAllQuestion(List<Question> questions) {
        questionRepository.saveAll(questions);
    }

    public Map<Long, Boolean> getQuizAnswers(long quizId) {
        List<Question> allByQuizId = questionRepository.findAllByQuizId(quizId);
        return allByQuizId.stream()
                .map(q -> q.getOptions())
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(o -> o.getId(), o -> o.getIsCorrect()));
    }
}
