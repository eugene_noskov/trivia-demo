package org.eugene.library.controllers.api.admin;

import lombok.RequiredArgsConstructor;
import org.eugene.library.entity.Option;
import org.eugene.library.entity.Question;
import org.eugene.library.entity.Quiz;
import org.eugene.library.services.QuestionService;
import org.eugene.library.services.QuizService;
import org.eugene.library.services.ResultService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@RepositoryRestController
@RequiredArgsConstructor
@RequestMapping("/api/trivia/v1/admin")
public class InitController {
    private final QuizService quizService;
    private final QuestionService questionService;
    private final ResultService resultService;

    @PostMapping("/init")
    public ResponseEntity<String> initDatabase() {
        cleanDatabase();
        fillDatabaseWithSampleData();
        return ResponseEntity.ok("Database initialized");
    }

    private void fillDatabaseWithSampleData() {
        Quiz quiz = new Quiz("Addition quiz!");
        quiz = quizService.saveQuiz(quiz);

        List<Question> questions = new ArrayList<>();
        Question question = new Question(quiz, "What is 1 + 1 ?")
                .addOption(new Option("2", true))
                .addOption(new Option("1", false))
                .addOption(new Option("5", false));
        questions.add(question);

        question = new Question(quiz, "What is 2 + 3 ?")
                .addOption(new Option("6", false))
                .addOption(new Option("-2", false))
                .addOption(new Option("5", true));

        questions.add(question);
        questionService.saveAllQuestion(questions);


        quiz = new Quiz("Boolean algebra quiz!");
        quiz = quizService.saveQuiz(quiz);

        questions.clear();
        question = new Question(quiz, "True and true is?")
                .addOption(new Option("false", false))
                .addOption(new Option("true", true))
                .addOption(new Option("truetrue", false));
        questions.add(question);

        question = new Question(quiz, "True or false is ?")
                .addOption(new Option("true", true))
                .addOption(new Option("false", false))
                .addOption(new Option("it is illegal", false));

        questions.add(question);
        questionService.saveAllQuestion(questions);

    }

    private void cleanDatabase() {
        quizService.deleteAll();
        questionService.deleteAll();
        resultService.deleteAll();
    }
}
