package org.eugene.library.controllers.api.user;

import lombok.RequiredArgsConstructor;
import org.eugene.library.entity.Quiz;
import org.eugene.library.services.QuizService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RepositoryRestController
@RequiredArgsConstructor
@RequestMapping("/api/trivia/v1/user/")
public class QuizUserController {

    private final QuizService quizService;

    @GetMapping("/quizzes")
    public ResponseEntity<List<Quiz>> getQuizzes() {
        List<Quiz> quizzes = quizService.getQuizzes();
        if (quizzes == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(quizzes);
    }
}
