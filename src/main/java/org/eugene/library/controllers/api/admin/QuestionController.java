package org.eugene.library.controllers.api.admin;

import lombok.RequiredArgsConstructor;
import org.eugene.library.entity.Question;
import org.eugene.library.services.QuestionService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RepositoryRestController
@RequiredArgsConstructor
@RequestMapping("/api/trivia/v1/admin/")
public class QuestionController {
    private final QuestionService questionService;

    @PostMapping("/quiz/{id}/question")
    public ResponseEntity<Question> addQuestionss(@PathVariable(name = "id") long id,
                                                  @Valid @RequestBody Question question) {
        Question savedQuestion = questionService.saveQuestion(id, question);
        if (savedQuestion == null) {
            return ResponseEntity.unprocessableEntity().build();
        }

        return ResponseEntity.ok().body(savedQuestion);
    }
}
