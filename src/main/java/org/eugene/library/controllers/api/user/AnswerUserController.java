package org.eugene.library.controllers.api.user;

import lombok.RequiredArgsConstructor;
import org.eugene.library.entity.Question;
import org.eugene.library.entity.Result;
import org.eugene.library.services.AnswerService;
import org.eugene.library.utils.Aouth2Util;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@RepositoryRestController
@RequiredArgsConstructor
@RequestMapping("/api/trivia/v1/user/")
public class AnswerUserController {
    private final AnswerService answerService;

    @PostMapping("/quiz/{quizId}/answers")
    public ResponseEntity<Result> saveQuizAnswers(
            Principal principal,
            @PathVariable("quizId") long quizId,
            @RequestBody List<Question> answers) {

        String useruUid = Aouth2Util.getUserDetail("sub", principal);

        Result saveResult = answerService.saveQuizAnswersResult(useruUid, quizId, answers);
        if (saveResult == null) {
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok().body(saveResult);
    }
}
