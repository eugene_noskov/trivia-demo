package org.eugene.library.controllers.api.user;

import lombok.RequiredArgsConstructor;
import org.eugene.library.entity.Result;
import org.eugene.library.services.ResultService;
import org.eugene.library.utils.Aouth2Util;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@RepositoryRestController
@RequiredArgsConstructor
@RequestMapping("/api/trivia/v1/user/")
public class ResultUserController {
    private final ResultService resultService;

    @GetMapping("/results")
    public ResponseEntity<List<Result>> getUserResults(Principal principal) {
        String userUid = Aouth2Util.getUserDetail("sub", principal);
        return ResponseEntity.ok().body(resultService.getUserResults(userUid));
    }
}
