package org.eugene.library.controllers.api.user;

import lombok.RequiredArgsConstructor;
import org.eugene.library.entity.Question;
import org.eugene.library.services.QuestionService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RepositoryRestController
@RequiredArgsConstructor
@RequestMapping("/api/trivia/v1/user/")
public class QuestionUserController {
    private final QuestionService questionService;

    @GetMapping("/quiz/{id}")
    public ResponseEntity<List<Question>> getQuiz(@PathVariable("id") long id) {
        List<Question> quizzes = questionService.getQuizQuestions(id);
        if (quizzes == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(quizzes);
    }
}
