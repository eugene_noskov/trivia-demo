package org.eugene.library.controllers.api.admin;

import lombok.RequiredArgsConstructor;
import org.eugene.library.entity.Quiz;
import org.eugene.library.services.QuizService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@RepositoryRestController
@RequiredArgsConstructor
@RequestMapping("/api/trivia/v1/admin/")
public class QuizController {

    private final QuizService quizService;

    @PostMapping("/quiz")
    public ResponseEntity<Quiz> addQuiz(@Valid @RequestBody Quiz quiz) {
        try {
            return ResponseEntity.ok().body(quizService.saveQuiz(quiz));
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    @GetMapping("/quizzes")
    public ResponseEntity<List<Quiz>> getQuizzes() {
        List<Quiz> quizzes = quizService.getQuizzes();
        if (quizzes == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(quizzes);
    }
}
