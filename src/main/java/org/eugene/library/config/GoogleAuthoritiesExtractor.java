package org.eugene.library.config;

import org.eugene.library.entity.User;
import org.eugene.library.repositories.UserDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class GoogleAuthoritiesExtractor implements AuthoritiesExtractor {

    @Autowired
    UserDetailsRepo userDetailsRepo;

    @Override
    public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {

        //For simplicity, first logined user to the app gets admin rights automatically
        String authorities = "USER";

        String id = (String) map.get("sub");
        Optional<User> userOpt = userDetailsRepo.findById(id);
        User user;
        if (!userOpt.isPresent()) {
            Long userNum = userDetailsRepo.tryFindAny();
            if (userNum == 0) {
                authorities = "USER,ADMIN";
            }
            user = createUser(map, id);
            user.setRoles(authorities);
        } else {
            user = userOpt.get();
            authorities = user.getRoles();
        }

        saveUser(user);

        return AuthorityUtils.commaSeparatedStringToAuthorityList(authorities);
    }

    private User createUser(Map<String, Object> map, String id) {
        User user = new User();
        user.setId(id);
        user.setName((String) map.get("name"));
        user.setEmail((String) map.get("email"));
        user.setGender((String) map.get("gender"));
        user.setLocale((String) map.get("locale"));
        user.setUserpic((String) map.get("picture"));

        return user;
    }


    private void saveUser(User user) {
        user.setLastVisit(LocalDateTime.now());
        userDetailsRepo.save(user);
    }
}
