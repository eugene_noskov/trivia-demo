package org.eugene.library.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "quiz")
@Data
@NoArgsConstructor
public class Quiz {
    @Id
    @GeneratedValue(generator = "auto")
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    public Quiz(String title) {
        this.title = title;
    }
}
