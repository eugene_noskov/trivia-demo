package org.eugene.library.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "question_option")
@EqualsAndHashCode(exclude = {"question"})
@ToString(exclude = {"question"})
public class Option {
    @Id
    @GeneratedValue(generator = "auto")
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "is_correct", nullable = false)
    private Boolean isCorrect;

    @ManyToOne
    @JoinColumn(name = "question_id")
    @JsonIgnore
    private Question question;

    public Option(String title, Boolean isCorrect) {
        this.title = title;
        this.isCorrect = isCorrect;
    }
}
