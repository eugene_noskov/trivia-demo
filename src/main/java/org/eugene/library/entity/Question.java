package org.eugene.library.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name = "question")
@EqualsAndHashCode(exclude = {"quiz", "options"})
@ToString(exclude = {"quiz", "options"})
@Accessors(chain = true)
public class Question {
    @Id
    @GeneratedValue(generator = "auto")
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "question", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private Set<Option> options = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "quiz_id", nullable = false)
    @JsonIgnore
    private Quiz quiz;

    public Question(Quiz quiz, String title) {
        this.title = title;
        this.quiz = quiz;
    }

    public Question addOption(Option option) {
        options.add(option);
        option.setQuestion(this);
        return this;
    }
}
