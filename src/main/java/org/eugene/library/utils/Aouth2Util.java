package org.eugene.library.utils;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.security.Principal;
import java.util.Map;

public class Aouth2Util {
    public static String getUserDetail(String detailName, Principal principal) {
        Map<String, String> userOuth2Details = getUserOuth2Details(principal);
        return userOuth2Details.get(detailName);
    }

    private static Map<String, String> getUserOuth2Details(Principal principal) {
        return (Map<String, String>) ((OAuth2Authentication) principal).getUserAuthentication().getDetails();
    }
}
