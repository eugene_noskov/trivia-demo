package org.eugene.library.repositories;

import org.eugene.library.entity.Quiz;
import org.eugene.library.entity.Result;
import org.eugene.library.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {

    List<Result> getByUser(User user);

    void deleteByUserAndQuiz(User user, Quiz quiz);
}
