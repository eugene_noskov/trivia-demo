package org.eugene.library.repositories;

import org.eugene.library.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserDetailsRepo extends JpaRepository<User, String> {

    Optional<User> findById(String s);

    @Query("SELECT Count(u) FROM User u")
    Long tryFindAny();
}
