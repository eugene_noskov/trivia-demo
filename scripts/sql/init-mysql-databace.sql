CREATE DATABASE `db_interview`;

USE  db_interview;

CREATE TABLE `auto` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `db_interview`.`auto`
(`next_val`)
VALUES
(20);


CREATE TABLE `question` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `quiz_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb0yh0c1qaxfwlcnwo9dms2txf` (`quiz_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `question_option` (
  `id` bigint(20) NOT NULL,
  `is_correct` bit(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `question_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmmdv54rmm5hkgxbn1008ix87n` (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `quiz` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `result` (
  `id` bigint(20) NOT NULL,
  `right_answers` int(11) DEFAULT NULL,
  `wrong_answers` int(11) DEFAULT NULL,
  `quiz_id` bigint(20) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK25wxfk8hlyt83jaa1no4anjsg` (`quiz_id`),
  KEY `FKc001svrmfl0aggh1q7ao32xhs` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `usr` (
  `id` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `userpic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
