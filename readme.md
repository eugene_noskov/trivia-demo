##### Trivia Demo Project

**Requirements:**

- MySql 5 database.
- Google e-mail

**Deployment**

- By default, server should be started on localhost otherwise Google Cloud Settings must be changed along with project security  settings in application.properties file.
- Table with name "db_interview" must be created. You cad do it manually using MySql Workbench or with sql script:
CREATE SCHEMA `db_interview` DEFAULT CHARACTER SET utf8 ;
- Filling  database with sample quiz' data can be accomplished with POST query:
    _http://localhost:8080/api/trivia/v1/admin/init_
    
**Description:**

- There two roles in project: administrator and user. First loggined user gets admin role.  
    Roles can be changed in "usr" table of database.

**Testing**

- To test API you can use HTTP queries for Postman. They can be found in group: 
    _http://trivia-demo.postman.co_ 
- To get token for testing in Postman it nessesary get access token _Query_ > Authorithation:
    - Type: OAuth 2
    - Access token:
        - Callback URL: http://localhost:8080/login
        - Auth URL: https://accounts.google.com/o/oauth2/v2/auth?access_type=offline
        - Access Token URL: https://www.googleapis.com/oauth2/v4/token
        - Client ID: 684799289081-trqjg29an3dl6csp26h657o9tfth4qad.apps.googleusercontent.com
        - Client Secret: t7tp9k-sEtvzfxq9OnfKrLUi
        - Scope: https://www.googleapis.com/auth/cloud-platform
        - Client Authentication: Send as basic in Auth header
        
- Test requests
    - Fill database with sample data: POST localhost:8080/api/trivia/v1/admin/init
    - ADMIN part
        - Add new quiz POST http://localhost:8080/api/trivia/v1/admin/quiz
        - Get all quizzes: GET http://localhost:8080/api/trivia/v1/admin/quizzes
        - Add question to quiz: POST http://localhost:8080/api/trivia/v1/admin/quiz/{quizId}/question
        - Getting user's results: GET http://localhost:8080/api/trivia/v1/user/results
    - USER
        - Get list of quizzes: GET http://localhost:8080/api/trivia/v1/user/quizzes
        - Get questions for particular quiz: GET http://localhost:8080/api/trivia/v1/user/quiz/{quizId}
        - Adding answers for particular quiz: POST http://localhost:8080/api/trivia/v1/user/quiz/{quizId}/answers
 